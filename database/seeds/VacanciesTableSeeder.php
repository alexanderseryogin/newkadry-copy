<?php

use App\Entities\Data\Vacancy;
use Illuminate\Database\Seeder;

class VacanciesTableSeeder extends Seeder
{

    protected $vacancies;

    public function __construct()
    {
        $this->vacancies = [
            [
                'title' => 'Ruby-разработчик',
                'description' => 'Описание',
                'salary' => 250,
                'currency_id' => 2,
                'city_id' => 1,
                'user_id' => 1
            ]
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Vacancy::insert($this->vacancies);
    }
}
