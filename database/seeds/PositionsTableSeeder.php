<?php

use App\Entities\Data\Position;
use Illuminate\Database\Seeder;

class PositionsTableSeeder extends Seeder
{

    protected $positions;


    public function __construct()
    {
        $this->positions = [
            ['title' => 'PHP Developer'],
            ['title' => 'iOS Developer'],
            ['title' => 'Ruby Developer'],
            ['title' => 'JS Developer']
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Position::insert($this->positions);
    }
}
