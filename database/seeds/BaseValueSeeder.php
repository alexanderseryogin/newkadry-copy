<?php

use App\Entities\Data\City;
use App\Entities\Data\Company;
use App\Entities\Vacancy;
use App\Entities\Data\Currency;
use App\Entities\Data\Position;
use Illuminate\Database\Seeder;
use App\Entities\Data\EventType;
use App\Entities\User;

/**
 * Class BaseValueSeeder
 */
class BaseValueSeeder extends Seeder
{

    /**
     * BaseValueSeeder constructor.
     */
    public function __construct()
    {

    }


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    }
}
