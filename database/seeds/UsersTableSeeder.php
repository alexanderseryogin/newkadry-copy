<?php

use App\Entities\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    protected $users;

    public function __construct()
    {
        $this->users = [
            [
                'first_name' => 'Николай',
                'last_name' => 'Задворный',
                'second_name' => '',
                'email' => 'nick.zadvorniy@faceit.com.ua',
                'password' => bcrypt('director'),
                'status' => User::STATUS_DIRECTOR,
                'phone' => '+380979696057',
                'skype' => 'kolyazdv',
            ],
            [
                'first_name' => 'Алексей',
                'last_name' => 'Доценко',
                'second_name' => '',
                'email' => 'alex.dotsenko@faceit.com.ua',
                'password' => bcrypt('admin'),
                'status' => User::STATUS_TEAM_LEAD,
                'phone' => '+380635626519',
                'skype' => 'alexeydotsenko',
            ],
            [
                'first_name' => 'Дмитрий',
                'last_name' => 'Оплачко',
                'second_name' => '',
                'email' => 'dmitry.oplachko@faceit.com.ua',
                'status' => User::STATUS_TEAM_LEAD,
                'password' => bcrypt('admin'),
                'phone' => '+380687465394',
                'skype' => 'debian1991',
            ],
            [
                'first_name' => 'Михаил',
                'last_name' => 'Беляев',
                'second_name' => '',
                'email' => 'mihail.belyaev@faceit-team.com',
                'password' => bcrypt('admin'),
                'status' => User::STATUS_TEAM_LEAD,
                'phone' => '+380991039043',
                'skype' => 'dark-def',
            ],
            [
                'first_name' => 'Оксана',
                'last_name' => 'Касич-Пилипенко',
                'second_name' => '',
                'email' => 'oksana.casich@faceit.com.ua',
                'password' => bcrypt('admin'),
                'status' => User::STATUS_HR,
                'phone' => '+380972906717',
                'skype' => 'oksana.casich',
            ],
            [
                'first_name' => 'Ирина',
                'last_name' => 'Кузьминова',
                'second_name' => '',
                'email' => 'irene.kuzminova@faceit.com.ua',
                'password' => bcrypt('admin'),
                'status' => User::STATUS_HR,
                'phone' => '+380662710648',
                'skype' => 'zp.kuzminova',
            ],
            [
                'first_name' => 'Admin',
                'last_name' => 'Super',
                'second_name' => null,
                'email' => 'super.admin@admin.com',
                'password' => bcrypt('admin'),
                'status' => User::STATUS_HR,
                'phone' => '+380991234567',
                'skype' => null,

            ],
            //a couple of users
            [
                'first_name' => 'Iван',
                'last_name' => 'Франко',
                'second_name' => 'Якович',
                'email' => 'ivan.franko@mail.if.ua',
                'password' => bcrypt('admin'),
                'status' => User::STATUS_CANDIDATE,
                'phone' => '+380991234567',
                'skype' => null,
            ],
            [
                'first_name' => 'Борис',
                'last_name' => 'Березовский',
                'second_name' => 'Абрамович',
                'email' => 'boris@berezovski.co.uk',
                'password' => bcrypt('admin'),
                'status' => User::STATUS_CANDIDATE,
                'phone' => '+380999876543',
                'skype' => 'boris_b',
            ],
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert($this->users);
    }
}
