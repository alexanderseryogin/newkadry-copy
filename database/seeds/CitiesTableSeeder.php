<?php

use App\Entities\Data\City;
use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    protected $cities;


    public function __construct()
    {
        $this->cities = [
            ['title' => 'Запорожье'],
            ['title' => 'Львов'],
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        City::insert($this->cities);
    }
}
