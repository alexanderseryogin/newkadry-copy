<?php

use Illuminate\Database\Seeder;
use App\Test;

class TestTableSeeder extends Seeder
{

    protected $times;

    public function __construct()
    {
        $this->times = [
            [
                'timestamp' => '12:00:00'
            ],
            [
                'timestamp' => '13:00:00'
            ],
            [
                'timestamp' => '14:00:00'
            ],
            [
                'timestamp' => '15:00:00'
            ],
        ];
    }



    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Test::insert($this->times);
    }
}
