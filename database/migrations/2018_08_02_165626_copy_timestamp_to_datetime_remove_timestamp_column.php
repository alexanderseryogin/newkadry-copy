<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CopyTimestampToDatetimeRemoveTimestampColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {

            //copy data from one column to another
            DB::update('
                UPDATE events
                SET `datetime` = `timestamp`
            ');

            // after all transformations we don't need `timestamp` column any more
            $table->dropColumn('timestamp');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {


            // if something went wrong, back `datetime` to default value (<null>)
            DB::update('
                UPDATE events
                SET `datetime` = null
            ');

            // if something went wrong, back the `timestamp` column with type `time`
            $table->time('timestamp');
        });
    }
}
