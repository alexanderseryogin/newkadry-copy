<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class TestCopyDataToAnotherColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    //copying data from `timestamp` column to `datetime` column
    public function up()
    {
        DB::update('
            UPDATE test 
            SET `datetime` = `timestamp` 
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::update('
            UPDATE test 
            SET `datetime` = null
        ');
    }
}
