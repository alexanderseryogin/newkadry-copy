<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id');
            $table->string('title')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('interview_id')->nullable();
            $table->integer('candidate_id')->nullable();
            $table->integer('position_id')->nullable();
            $table->integer('hr_id')->nullable();
            $table->integer('vacancy_id')->nullable();
            $table->integer('currency_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('company_id')->nullable();
            $table->text('description')->nullable();
            $table->time('timestamp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
