<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDatetimeColumnTypeDatetimeTransferDataRemoveTimestamp extends Migration
{
    // ChangeEventTimestampTypeFromTimeTo____DATETIME  <=
    // actually we change the type form TIME to DATETIME

    // WARNING! IN THIS MIGRATION JUST NEW COLUMN `datetime` WAS ADDED.

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dateTime('timestamp')->change();

            /*===============ADDED===================*/
            $table->dateTime('datetime')->after('timestamp')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->time('timestamp')->change();

            /*===============ADDED===================*/
            $table->dropColumn('datetime');


        });
    }
}
