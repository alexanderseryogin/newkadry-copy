<?php

namespace App\Http\Requests\Interview;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'sometimes',
            'candidate_id' => 'sometimes',
            'position_id' => 'sometimes',
            'vacancy_id' => 'sometimes',
            'interviewer_id' => 'sometimes',
            'city_id' => 'sometimes',
            'date_from' => 'sometimes',
            'date_to' => 'sometimes'
        ];
    }
}
