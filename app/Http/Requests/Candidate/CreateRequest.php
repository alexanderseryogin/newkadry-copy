<?php

namespace App\Http\Requests\Candidate;

use App\Entities\Data\DataDTO;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use \App\Http\Requests\User\CreateRequest as UserCreateRequest;

/**
 * Class CreateRequest
 */
class CreateRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        $data = new DataDTO();
        return UserCreateRequest::getRules() + [
            'position' => ['required', 'integer', Rule::in(array_keys($data->positionsList))],
            'salary' => 'nullable|integer',
            'currency' => ['nullable', 'integer', Rule::in(array_keys($data->currenciesList))],
            'city' => ['required', 'integer', Rule::in(array_keys($data->citiesList))],
            'company' => ['nullable', 'integer', Rule::in(array_keys($data->companiesList))],
            'cv_src' => 'nullable|url',
            'cv_file' => 'nullable|file|mimes:pdf,doc,docx,rtf,odt',
            'languages' => 'nullable|string',
            'description' => 'nullable|string',
            'notes' => 'nullable|string',
        ];
    }


}