<?php
namespace App\Http\Controllers\Data;

use App\Entities\Data\Position;
use App\Event;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Validation\Rule;

/**
 * Class PositionController
 */
class PositionController
{
    /**
     * @return View
     */
    public function index()
    {
        $positions = Position::orderBy('title')->paginate(10);
        return view('data.position.index', compact('positions'));
    }

    /**
     * @return View
     */
    public function create()
    {
        return view('data.position.create');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:255|unique:positions',
        ]);

        Position::create(['title' => $request['title']]);

            Event::add(7, $request['title']);

        return redirect()->route('data.position.index');
    }

    /**
     * @param Position $position
     * @return View
     */
    public function edit(Position $position)
    {
        return view('data.position.edit', compact('position'));
    }

    /**
     * @param Request $request
     * @param Position $position
     * @return RedirectResponse
     */
    public function update(Request $request, Position $position)
    {
        $request->validate([
            'title' => [
                'required', 'string', 'max:255', Rule::unique('positions')->ignore($position->id),
            ]]);
        $position->update(['title' => $request['title']]);

        return redirect()->route('data.position.index');
    }

    /**
     * @param Position $position
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Position $position)
    {
        $position->delete();
        return redirect()->route('data.position.index');
    }
}