<?php
namespace App\Http\Controllers\Data;

use App\Entities\Data\City;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Validation\Rule;

/**
 * Class CityController
 */
class CityController
{
    /**
     * @return View
     */
    public function index()
    {
        $cities = City::orderBy('title')->paginate(10);
        return view('data.city.index', compact('cities'));
    }

    /**
     * @return View
     */
    public function create()
    {
        return view('data.city.create');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:255|unique:cities',
        ]);

        City::create(['title' => $request['title']]);

        return redirect()->route('data.city.index');
    }

    /**
     * @param City $city
     * @return View
     */
    public function edit(City $city)
    {
        return view('data.city.edit', compact('city'));
    }

    /**
     * @param Request $request
     * @param City $city
     * @return RedirectResponse
     */
    public function update(Request $request, City $city)
    {
        $request->validate([
            'title' => [
                'required', 'string', 'max:255', Rule::unique('cities')->ignore($city->id),
            ]]);
        $city->update(['title' => $request['title']]);

        return redirect()->route('data.city.index');
    }

    /**
     * @param City $city
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(City $city)
    {
        $city->delete();
        return redirect()->route('data.city.index');
    }
}