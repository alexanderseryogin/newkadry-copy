<?php
namespace App\Http\Controllers\Data;

use App\Entities\Data\City;
use App\Entities\Data\Company;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Validation\Rule;

/**
 * Class CompanyController
 */
class CompanyController
{
    /**
     * @return View
     */
    public function index()
    {
        $companies = Company::with('city')->orderBy('companies.title')->paginate(10);
        return view('data.company.index', compact('companies'));
    }

    /**
     * @return View
     */
    public function create()
    {
        $citiesList = City::getAllCities();
        return view('data.company.create', compact('citiesList'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string|max:255|unique:companies',
            'city' => 'required|integer|exists:cities,id',
            'about' => 'string'
        ]);

        Company::create([
            'title' => $request['title'],
            'city_id' => $request['city'],
            'about' => $request['about']
        ]);

        return redirect()->route('data.company.index');
    }

    /**
     * @param Company $company
     * @return View
     */
    public function edit(Company $company)
    {
        $citiesList = City::getAllCities();
        return view('data.company.edit', compact('company', 'citiesList'));
    }

    /**
     * @param Request $request
     * @param Company $company
     * @return RedirectResponse
     */
    public function update(Request $request, Company $company)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255', Rule::unique('companies')->ignore($company->id)],
            'city' => 'required|integer|exists:cities,id',
            'about' => 'string'
        ]);

        $company->update([
            'title' => $request['title'],
            'city_id' => $request['city'],
            'about' => $request['about']
        ]);

        return redirect()->route('data.company.index');
    }

    /**
     * @param Company $company
     * @return View
     */
    public function show(Company $company)
    {
        return view('data.company.show', compact('company'));
    }

    /**
     * @param Company $Company
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Company $company)
    {
        $company->delete();
        return redirect()->route('data.company.index');
    }


}