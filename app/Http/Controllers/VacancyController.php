<?php

namespace App\Http\Controllers;

use App\Entities\Data\DataDTO;
use App\Entities\Vacancy;
use App\Http\Requests\Vacancy\CreateRequest;
use App\Http\Requests\Vacancy\UpdateRequest;
use App\Services\Vacancy\VacancyService;
use Illuminate\Contracts\View\View;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class VacancyController
 */
class VacancyController extends Controller
{
    /**
     * @var VacancyService
     */
    private $service;

    /**
     * VacancyController constructor.
     * @param VacancyService $service
     */
    public function __construct(VacancyService $service)
    {
        $this->service = $service;
        $this->middleware('can:manage-vacancies')->except(['index', 'show']);
    }

    /**
     * @return View
     */
    public function index(): View
    {
        $vacancies = Vacancy::orderBy('updated_at')->with('city')->paginate(10);
        return view('vacancy.index', compact('vacancies'));
    }

    /**
     * @return View
     */
    public function create(): View
    {
        $data = new DataDTO();
        return view('vacancy.create', compact('data'));
    }

    /**
     * @param CreateRequest $request
     * @return RedirectResponse
     */
    public function store(CreateRequest $request): RedirectResponse
    {
        try {
            $this->service->createVacancy($request);
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }

        return redirect()->route('vacancy.index');
    }

    /**
     * @param Vacancy $vacancy
     * @return View
     */
    public function edit(Vacancy $vacancy): View
    {
        $data = new DataDTO();
        return view('vacancy.edit', compact('vacancy', 'data'));
    }

    /**
     * @param UpdateRequest $request
     * @param Vacancy $vacancy
     * @return RedirectResponse
     */
    public function update(UpdateRequest $request, Vacancy $vacancy): RedirectResponse
    {
        /*$vacancy = Vacancy::find()->where(['id' => $id])->one();
        $vacancy->name=$request->name();*/


        try {
            $this->service->updateVacancy($request, $vacancy);
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }

        return redirect()->route('vacancy.show', $vacancy);
    }

    /**
     * @param Vacancy $vacancy
     * @return View
     */
    public function show(Vacancy $vacancy): View
    {
        return view('vacancy.show', compact('vacancy'));
    }

    /**
     * @param Vacancy $vacancy
     * @return RedirectResponse
     */
    public function destroy(Vacancy $vacancy): RedirectResponse
    {
        try {
            $this->service->deleteVacancy($vacancy);
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('vacancy.index');
    }
}
