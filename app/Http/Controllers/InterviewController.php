<?php

namespace App\Http\Controllers;

use App\Debug;
use App\Entities\Candidate;
use App\Entities\Data\City;
use App\Entities\Data\DataDTO;
use App\Entities\Data\Position;
use App\Entities\Data\Vacancy;
use App\Entities\User;
use App\Entities\Data\Organizer;
use App\Http\Requests\Interview\InterviewRequest;
use App\Http\Requests\Interview\UpdateRequest;
use App\Interview;
use App\InterviewEmployee;
use App\Services\Interview\InterviewService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class InterviewController extends Controller
{
    /**
     * @var InterviewService
     */
    private $service;

    /**
     * InterviewController constructor.
     * @param InterviewService $service
     */
    public function __construct(InterviewService $service)
    {
        $this->service = $service;
        $this->middleware('can:manage-users')->except(['index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Interview $interview)
    {
        $interviews = Interview::orderBy('date_from', 'asc')->paginate(5);

        $usersIds = $interviews->pluck("candidate_id");
        $users = User::whereIn('id', $usersIds)->get()->keyBy("id");

        $positionsIDs = $interviews->pluck("position_id")->unique()->filter();
        $positions = Position::whereIn('id', $positionsIDs)->get()->keyBy("id");

        $vacanciesIDs = $interviews->pluck("vacancy_id")->unique()->filter();
        $vacancies = Vacancy::whereIn('id', $vacanciesIDs)->get()->toArray();

        $citiesIDs = $interviews->pluck("city_id")->unique()->filter();
        $cities = City::whereIn('id', $citiesIDs)->get()->keyBy("id");



        return view('interview.index', compact('candidates', 'interviews', 'interview_query',
        'users', 'positions', 'vacancies', 'cities', 'positions', 'allInterviewersIDArray'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = new DataDTO();
        return view('interview.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->service->createInterview($request);
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
        return redirect()->route('interview.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Interview $interview): View
    {
        $interview_id = (int)$interview->id;

        $interview = Interview::with(['user' => function ($query) use ($interview_id) {

            }])
            ->where('id', $interview_id)
            ->with('interviewEmployee')
//            ->with('user')
            ->get();

//        Debug::all($interview[0]['user']);

        $allInterviewersIDArray = InterviewEmployee::all()                              // get all the data
        ->where('interview_id', $interview_id)
            ->toArray();
        $allInterviewersIDArray = array_pluck($allInterviewersIDArray, 'user_id'); // get just purified `user_id` [0=>1, 1=>3, 2=>7]

        return \view('interview.show', compact('interview', 'interview_id', 'allInterviewersIDArray'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Interview $interview): View
    {
//        dd($interview);

        $interview_id = (int)$interview->id;
        $interview = Interview::with(['user' => function(){

        }])
            ->with('candidate')
            ->with('city')
            ->with('position')
            ->with('vacancy')
            ->with('interviewEmployee')
            ->where('id', $interview_id)
            ->get();

        $interview = $interview[0];

        $data = new DataDTO();

        $allInterviewersIDArray = InterviewEmployee::all()                              // get all the data
            ->where('interview_id', $interview_id)
            ->toArray();
        $allInterviewersIDArray = array_pluck($allInterviewersIDArray, 'user_id'); // get just purified `user_id` [0=>1, 1=>3, 2=>7]

        return view('interview.edit', compact('data', 'interview_id', 'interview', 'allInterviewersIDArray'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Interview $interview, InterviewEmployee $interviewEmployee)
    {
        /*---UPDATE/DELETE---*/
//        try
//        {
//            $interview->update($request->all());
//            //remove all old interviewers_id related with this interview
//            DB::transaction(function () use ($interview, $request)
//            {
//                DB::delete("DELETE FROM interview_employees WHERE interview_id = ?", [$interview->id]);
//                    foreach ($request->interview_employees_ids as $user_id)
//                    {// insert new interviewers_id
//                        DB::insert("INSERT INTO interview_employees(interview_id, user_id) VALUES (?, ?)", [$interview->id, $user_id]);
//                    }
//            });
//        } catch (\Exception $e) {
//            return back()->with('error', $e->getMessage());
//        }


        try {
            $interview->update($request->all()); // update info in `interviews` table
            $interview->interviewEmployee()->sync($request->interview_employees_ids); // update(sync) info in `interview_employees` table
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }



        //change related entry in `organizer` table
        try {
            DB::table('organizer')
                ->where('interview_id', '=', $request->only('interview_id'))
                ->update(
                    [
                        'start' => $interview->date_from,
                        'end' => $interview->date_from
                    ]);

        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }

        return redirect()->route('interview.show', $interview);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Interview $interview): RedirectResponse
    {
        $interview->delete();
        return redirect()->back();
    }

    public function remove($id)
    {

    }
}
