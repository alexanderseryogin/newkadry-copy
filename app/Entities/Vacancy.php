<?php

namespace App\Entities;

use App\Entities\Data\City;
use App\Entities\Data\Currency;
use App\Event;
use App\Interview;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Vacancy
 */
class Vacancy extends Model
{
    /**
     * @var array
     */
    protected $guarded = ['id'];

    protected $table = 'vacancies';

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function currency(): BelongsTo
    {
        return $this->belongsTo(Currency::class, 'currency_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    public function event()
    {
        return $this->hasOne(Event::class, 'vacancy_id', 'id');
    }

    public function interview()
    {
        return $this->hasOne(Interview::class, 'vacancy_id', 'id');
    }

}
