<?php

namespace App\Entities\Data;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Position
 */
class Position extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['title'];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return array
     */
    public static function getAllPositions(): array
    {
        static $cache = null;

        if($cache === null) {
            $cache =  self::pluck('title', 'id')->toArray();
        }

        return $cache;
    }

    public function event()
    {
        return $this->hasOne(Event::class, 'position_id', 'id');
    }

    public static function getPositionOnID($id){
        $title = Position::all();
        return $title;
    }
}
