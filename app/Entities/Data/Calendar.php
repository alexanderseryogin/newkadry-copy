<?php

namespace App\Entities\Data;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    //

    protected $table = 'calendars';

    protected $fillable = ['start', 'end', 'title', 'description', 'color'];

    public $timestamps = false;
}
