<?php

namespace App\Entities\Data;

use Illuminate\Database\Eloquent\Model;

/**
 * Class City
 */
class City extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['title'];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return array
     */
    public static function getAllCities(): array
    {
        return self::pluck('title', 'id')->toArray();
    }

    public function event(){
        return $this->hasOne(Event::class, 'event_id', 'id');
    }
}
