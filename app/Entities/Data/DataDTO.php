<?php

namespace App\Entities\Data;
use App\Entities\Candidate;
use App\Entities\User;
use App\Event;
use App\Interview;

/**
 * Class DataDTO
 */
class DataDTO
{
    /**
     * @var array
     */
    public $positionsList;

    /**
     * @var array
     */
    public $currenciesList;

    /**
     * @var array
     */
    public $citiesList;

    /**
     * @var array
     */
    public $companiesList;

    /**
     * @var array
     */
    public $vacanciesList;

    /**
     * @var array
     */
    public $usersList;

    /**
     * @var array
     */
    public $usersHRList;

    /**
     * @var array
     */
//    public $allInterviewersList;

    /**
     * DataDTO constructor.
     */
    public function __construct()
    {
        $this->positionsList = Position::getAllPositions();
        $this->companiesList = Company::getAllCompanies();
        $this->currenciesList = Currency::getAllCurrencies();
        $this->citiesList = City::getAllCities();
        $this->vacanciesList = Vacancy::getAllVacancies();
        $this->usersList = User::getAllUsers();
        $this->usersHRList = User::getAllHRUsers();
//        $this->allInterviewersList = User::getAllIntreviewersList();
        $this->candidatesList = Candidate::getAll();
        $this->interviewsList = Interview::getAll();
        $this->eventsList = Event::getAll();
    }
}