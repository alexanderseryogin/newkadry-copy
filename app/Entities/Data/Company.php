<?php

namespace App\Entities\Data;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Company
 */
class Company extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['title', 'city_id', 'about'];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return BelongsTo
     */
    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    /**
     * @return array
     */
    public static function getAllCompanies(): array
    {
        return self::pluck('title', 'id')->toArray();
    }
}
