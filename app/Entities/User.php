<?php

namespace App\Entities;

use App\InterviewEmployee;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

/**
 * Class User
 */
class User extends Authenticatable
{
    use Notifiable;

    const STATUS_GOT_OFFER = -2;
    const STATUS_FIRED = -1;
    const STATUS_HR = 1;
    const STATUS_INTERVIEWER = 2;
    const STATUS_CANDIDATE = 3;
    const STATUS_EMPLOYEE = 4;
    const STATUS_TEAM_LEAD = 5;
    const STATUS_DIRECTOR = 100;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'second_name',
        'email',
        'password',
        'photo',
        'socials',
        'education',
        'status',
        'phone',
        'skype',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return bool
     */
    public function isCandidate(): bool
    {
        return $this->status === self::STATUS_CANDIDATE;
    }

    /**
     * @return bool
     */
    public function isHR(): bool
    {
        return $this->status === self::STATUS_HR;
    }

    /**
     * @return bool
     */
    public function setEmployeeStatus(): bool
    {
        return $this->update(['status' => self::STATUS_EMPLOYEE]);
    }

    /**
     * @return bool
     */
    public function setCandidateStatus(): bool
    {
        return $this->update(['status' => self::STATUS_CANDIDATE]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function candidatesHistory()
    {
        return $this->hasMany(Candidate::class, 'user_id', 'id')->orderBy('updated_at', 'desc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function candidate()
    {
        return $this->hasOne(Candidate::class, 'user_id', 'id')->orderBy('candidates.updated_at', 'desc');
    }

    public function event()
    {
        return $this->hasOne(Event::class, 'user_id', 'id');
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeCandidatesStatus(Builder $builder)
    {
        return $builder->where('users.status', self::STATUS_CANDIDATE);
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeWorkersStatus(Builder $builder)
    {
        return $builder->where('users.status', '!=', self::STATUS_CANDIDATE);
    }

    /**
     * @return array
     */
    public static function statusesList(): array
    {
        return [
            self::STATUS_GOT_OFFER => 'Дали оффер',
            self::STATUS_FIRED => 'Уволен',
            self::STATUS_HR => 'HR',
            self::STATUS_INTERVIEWER => 'Проводящий интервью',
            self::STATUS_CANDIDATE => 'Кандидат',
            self::STATUS_EMPLOYEE => 'Работник',
            self::STATUS_TEAM_LEAD => 'Team-lead',
            self::STATUS_DIRECTOR => 'Директор'
        ];
    }

    public function interviewEmployee()
    {
        return $this->hasOne(InterviewEmployee::class, 'user_id', 'id');
    }


    /**
     * @param null|string $name
     * @return mixed
     */
    public static function findUsersWithCandidateInfo(?string $name)
    {
        $query = User::candidatesStatus()
            ->with(['candidate' => function ($query) { // подгружаем связь candidate() -> hasOne
                $query->with('recruiter', 'position', 'city', 'currency'); // у которого берём перечисленные позиции
            }]);
        if ($name) {
            $query->where(function ($query) use ($name) {
                $query->where('first_name', $name)
                    ->orWhere('second_name', $name)
                    ->orWhere('last_name', $name);
            });
        }
        return $query->orderBy('users.updated_at', 'desc')
            ->paginate(10);
    }

    /**
     * @return bool
     */
    public function updateUpdatedAt()
    {
        return $this->update(['updated_at' => Carbon::now()]);
    }

    public static function getAllUsers()
    {
        //return self::pluck('id', 'first_name', 'last_name', 'id')->toArray();
        $users = User::all('id', 'first_name', 'last_name');
        return $users;
    }

    public static function getAllHRUsers()
    {
        $users = DB::table('users')
            ->select('id', 'first_name', 'last_name')
            ->where('status', '=', 1)   //    + HR
            ->orWhere('status', '=', 5) // + TEAM_LEADS
            ->orWhere('status', '=', 100) // + DIRECTOR
            ->get();

        return $users;
    }




    public static function _findUsersWithCandidateInfo()
    {
        $query = User::candidatesStatus()
            ->with(['candidate' => function ($query) { // подгружаем связь candidate() -> hasOne
                $query->with('recruiter', 'position', 'city', 'currency'); // у которого берём перечисленные позиции
            }]);

        return $query;
    }


    public static function getUserById($id){
        $users = DB::table('users')
            ->where('id', '=', $id)
            ->get();

        return $users[0]->first_name . ' ' . $users[0]->last_name . ', ';
    }


    public function scopeLookEverywhere($query, $name)
    {
        $name = htmlspecialchars($name);
        return $query->where('last_name', 'like', '%'.$name.'%')
            ->orWhere('first_name', 'like', '%'.$name.'%')
            ->orWhere('second_name', 'like', '%'.$name.'%')
            ->orWhere('email', 'like', '%'.$name.'%')
            ->orWhere('phone', 'like', '%'.$name.'%')
            ->orWhere('skype', 'like', '%'.$name.'%');
    }

}
