<?php

namespace App\Services\User;

use App\Entities\Candidate;
use App\Entities\User;
use App\Event;
use App\Http\Requests\Candidate\CreateCandidateRecordRequest;
use App\Http\Requests\Candidate\CreateRequest;
use App\Http\Requests\Candidate\UpdateRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class CandidateService
 */
class CandidateService
{
    /**
     * @var UserService
     */
    private $service;

    /**
     * CandidateService constructor.
     * @param UserService $service
     */
    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    /**
     * @param CreateRequest $data
     */
    public function createUser(CreateRequest $data): void
    {
        $creatorId = Auth::user()->id;

        DB::transaction(function () use ($data, $creatorId) {

            $user = $this->service->createUser($data, true);
            $user->setCandidateStatus();

            $user->candidatesHistory()->create([
                'recruiter_id' => $creatorId,
                'position_id' => $data['position'],
                'salary' => $data['salary'],
                'currency_id' => $data['currency'],
                'city_id' => $data['city'],
                'cv_src' => $data['cv_src'],
                'cv_file' => $data->file('cv_file') ? $data->file('cv_file')->store('cvs', 'public') : null,
                'languages' => $data['languages'],
                'description' => $data['description'],
                'notes' => $data['notes'],
                'last_company_id' => $data['company'],
                'status' => Candidate::STATUS_DIALOG_OPEN
            ]);

            $event = new Event();
            $event->event_id = 2;
            $event->title = 'Добавлен новый кандидат';
            $event->user_id = $user->id;
            $event->city_id = $data['city'];
            $event->position_id = $data['position'];
            $data->currency_id = $data['currency'];
            $data->hr_id = $creatorId;
            $event->timestamp = date('Y-m-d H:i:s');
            $event->save();

            return $user;

        });
    }

    /**
     * @param Candidate $candidate
     * @param int $status
     */
    public function changeStatus(Candidate $candidate, int $status): void
    {
        if (!in_array($status, array_keys(Candidate::statusesList()))) {
            throw new \DomainException('Status not allowed');
        }
        $lastCandidate = Candidate::where('user_id', $candidate->user_id)
            ->orderBy('updated_at', 'desc')
            ->limit(1)
            ->first();
        if ($lastCandidate->id !== $candidate->id) {
            throw new \DomainException('This candidate record was expired');
        }
        $candidate->update(['status' => $status]);

        if ($candidate->isAccepted()) {
            $candidate->user->setEmployeeStatus();
        }
        $candidate->user->updateUpdatedAt();
    }

    /**
     * @param UpdateRequest $data
     * @param User $user
     */
    public function updateCandidate(UpdateRequest $data, User $user): void
    {
        $updaterId = Auth::user()->id;
        $file = $data->file('cv_file') ? $data->file('cv_file')->store('cvs', 'public') : $user->candidate->cv_file;

        $user->candidate()->update([
            'recruiter_id' => $updaterId,
            'position_id' => $data['position'],
            'salary' => $data['salary'],
            'currency_id' => $data['currency'],
            'city_id' => $data['city'],
            'cv_src' => $data['cv_src'],
            'cv_file' => $file,
            'languages' => $data['languages'],
            'description' => $data['description'],
            'notes' => $data['notes'],
            'last_company_id' => $data['company']
        ]);
        $user->updateUpdatedAt();
    }

    /**
     * @param CreateCandidateRecordRequest $data
     * @param User $user
     */
    public function createCandidateRecord(CreateCandidateRecordRequest $data, User $user): void
    {
        $creatorId = Auth::user()->id;

        $user->candidatesHistory()->create([
            'user_id' => $user->id,
            'recruiter_id' => $creatorId,
            'position_id' => $data['position'],
            'salary' => $data['salary'],
            'currency_id' => $data['currency'],
            'city_id' => $data['city'],
            'cv_src' => $data['cv_src'],
            'cv_file' => $data->file('cv_file') ? $data->file('cv_file')->store('cvs', 'public') : null,
            'languages' => $data['languages'],
            'description' => $data['description'],
            'notes' => $data['notes'],
            'last_company_id' => $data['company'],
            'status' => Candidate::STATUS_DIALOG_OPEN
        ]);
        $user->updateUpdatedAt();
    }
}