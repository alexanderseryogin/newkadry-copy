<?php
namespace App\Services\User;

use App\Entities\User;
use App\Event;
use App\Http\Requests\User\CreateRequest;
use App\Http\Requests\User\UpdateRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserService
 */
class UserService
{
    /**
     * @param User $user
     * @param int $status
     */
    public function changeStatus(User $user, int $status): void
    {
        if (!in_array($status, array_keys(User::statusesList()))) {
            throw new \DomainException('Status not allowed');
        }
        $user->update(['status' => $status]);
//        $user->status = $status;
//        $user->saveOrFail();
    }

    /**
     * @param UpdateRequest $request
     * @param User $user
     */
    public function updateUserInfo(UpdateRequest $request, User $user): void
    {

        $photo = $request->file('photo') ? $request->file('photo')->store('photos', 'public') : $user->photo;


       /* $user->update(
            array_merge($request->only
            ([
                'first_name',
                'last_name',
                'second_name',
                'email',
                'socials',
                'education',
                'skype',
                'phone'
            ]),
                ['photo' => $photo]
            )
        );*/

        $user->update(array_merge($request->except(['photo']), ['photo' => $photo]));

    }

    /**
     * @param Request $data
     * @param bool $isCandidate
     * @return User
     */
    public function createUser(Request $data, bool $isCandidate = false): User
    {
        $photo = $data->file('photo') ? $data->file('photo')->store('storage/photos', 'public') : null;

        $user = new User([
            "photo" => $photo,
        ] + $data->all());

        $user->status = $isCandidate ? User::STATUS_CANDIDATE : User::STATUS_EMPLOYEE;

        $user->save();

        $event = new Event();
        $event->event_id = 1;
        $event->title = 'Добавлен новый сотрудник';
        $event->hr_id = Auth::user()->id;
        $event->user_id = $user->id;
        $event->timestamp = date('Y-m-d H:i:s');
        $event->save();

        return $user;
    }
}