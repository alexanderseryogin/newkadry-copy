<?php

namespace App\Providers;

use App\Entities\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('use-site', function (User $user) {
            return !$user->isCandidate();
        });

        Gate::define('manage-users', function (User $user) {
            return $user->isHR();
        });

        Gate::define('edit-user-data', function (User $user, User $userEdit) {
            return $user->isHR() || ($user->id == $userEdit->id);
        });

        Gate::define('manage-vacancies', function (User $user) {
            return $user->isHR();
        });
    }
}
