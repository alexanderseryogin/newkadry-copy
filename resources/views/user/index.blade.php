@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Сотрудники
                    <a href="{{ route('home') }}" class="close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>

                <div class="card-body">
                    @can('manage-users')
                        <p><a href="{{ route('user.create') }}" class="btn btn-success">Добавить сотрудника</a></p>
                    @endcan
                    <!-- USER SEARCH -->
                    <form action="?" method="get">
                        <input name="name" type="text" placeholder="Введите, что помните" value="{{request('name')}}">
                        <input type="submit" value="Найти сотрудника">
                    </form>
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Дата</th>
                            <th>Имя</th>
                            <th>E-mail</th>
                            <th>Статус</th>
                            <th>Фото</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td>{{$user->created_at}}</td>
                                <td><a href="{{ route('user.show', $user->id) }}">{{$user->last_name ?? '' }} {{ $user->first_name ?? '' }} {{ $user->second_name ?? '' }}</a></td>
                                <td>{{$user->email}}</td>
                                <td><span class="badge badge-primary">{{$statusesList[$user->status]}}</span></td>
                                <td>
                                    @if($user->photo)
                                        <img class="little-photo" src="{{ asset('storage/' . $user->photo) }}" />
                                    @endif
                                </td>
                                <td>
                                    @can('manage-users')
                                        <div class="d-flex flex-row">
                                            <a href="{{ route('user.show', $user) }}" class="btn btn-sm btn-primary mr-1">Детали</a>
                                            <form method="POST" action="{{ route('user.destroy', $user) }}">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-sm btn-danger mr-1">Удалить</button>
                                            </form>
                                        </div>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>

@endsection