@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Создание нового кандидата
                    <a href="{{ route('user.show', $user) }}" class="close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('candidate.storeCandidateRecord', $user) }}" enctype="multipart/form-data">
                        @csrf
                        @include('candidate._candidate-create-form-fields', ['data' => $data])

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection