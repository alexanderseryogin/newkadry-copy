@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Создание новой вакансии
                    <a href="{{ route('vacancy.index') }}" class="close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('vacancy.store') }}">
                        @csrf

                        <div class="form-group">
                            <label for="title" class="col-form-label required">Заголовок</label>
                            <input id="title" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}" required>
                            @if ($errors->has('title'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('title') }}</strong></span>
                            @endif
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="salary" class="col-form-label">Зарплата</label>
                                    <input id="salary" class="form-control{{ $errors->has('salary') ? ' is-invalid' : '' }}" name="salary" value="{{ old('salary') }}">
                                    @if ($errors->has('salary'))
                                        <span class="invalid-feedback"><strong>{{ $errors->first('salary') }}</strong></span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="currency" class="col-form-label">Валюта</label>
                                    <select id="currency" class="form-control{{ $errors->has('currency') ? ' is-invalid' : '' }}" name="currency">
                                        <option value=""></option>
                                        @foreach ($data->currenciesList as $key => $value)
                                            <option value="{{ $key }}"{{ $key == old('currency') ? ' selected' : '' }}>
                                                {{ $value }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="city" class="col-form-label required">Город</label>
                            <select id="city" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city">
                                @foreach ($data->citiesList as $key => $value)
                                    <option value="{{ $key }}"{{ $key == old('city') ? ' selected' : '' }}>
                                        {{ $value }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="description" class="col-form-label required">Описание</label>
                            <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" rows="5" required>{{ old('description') }}</textarea>
                            @if ($errors->has('description'))
                                <span class="invalid-feedback"><strong>{{ $errors->first('description') }}</strong></span>
                            @endif
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection