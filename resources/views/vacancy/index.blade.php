@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Вакансии
                    <a href="{{ route('home') }}" class="close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>

                <div class="card-body">
                    @can('manage-vacancies')
                        <p><a href="{{ route('vacancy.create') }}" class="btn btn-success">Добавить вакансию</a></p>
                    @endcan
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Дата</th>
                            <th>Заголовок</th>
                            <th>Город</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($vacancies as $vacancy)
                            <tr>
                                <td>{{$vacancy->created_at}}</td>
                                <td><a href="{{ route('vacancy.show', $vacancy->id) }}">{{$vacancy->title}}</a></td>
                                <td>{{$vacancy->city->title}}</td>
                                <td>
                                    @can('manage-vacancies')
                                        <div class="d-flex flex-row">
                                            <a href="{{ route('vacancy.show', $vacancy) }}" class="btn btn-sm btn-primary mr-1">Детали</a>
                                            <form method="POST" action="{{ route('vacancy.destroy', $vacancy) }}">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-sm btn-danger mr-1">Удалить</button>
                                            </form>
                                        </div>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    {{ $vacancies->links() }}
                </div>
            </div>
        </div>
    </div>

@endsection