@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('vacancy.index') }}" class="close"><span aria-hidden="true">&times;</span></a>
                    <h3>Вакансия: {{$vacancy->title}}</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <p>
                                @can('manage-vacancies')
                                    <a href="{{ route('vacancy.edit', $vacancy) }}" class="btn btn-success">Редактировать</a>
                                @endcan
                            </p>
                            <table class="table table-bordered table-striped">
                                <tbody>
                                    <tr>
                                        <th>Заголовок</th><td>{{ $vacancy->title }}</td>
                                    </tr>
                                    <tr>
                                        <th>Зарплата</th><td>{{ $vacancy->salary . ' ' . ($vacancy->currency_id ? $vacancy->currency->title : '') }}</td>
                                    </tr>
                                    <tr>
                                        <th>Город</th><td>{{ $vacancy->city_id ? $vacancy->city->title : ''}}</td>
                                    </tr>
                                    <tr>
                                        <th>Описание</th><td>{{ $vacancy->description }}</td>
                                    </tr>
                                    <tr>
                                        <th>Добавил</th><td>{{ $vacancy->user->first_name . ' ' . $vacancy->user->last_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Добавлено</th><td>{{ $vacancy->created_at }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection