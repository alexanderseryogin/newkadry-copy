@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <center><h3>Лента событий</h3></center>
                    <br>
                    По периоду
                    <select id="homepageSelectEventInterval">
                        <option value="/" {{$type == '/' ? ' selected' : ''}}>Все события</option>
                        <option value="/?type=today" {{$type == 'today' ? ' selected' : ''}}>Сегодня</option>
                        <option value="/?type=yesterday" {{$type == 'yesterday' ? ' selected' : ''}}>Вчера</option>
                        <option value="/?type=week" {{$type == 'week' ? ' selected' : ''}}>Эта неделя</option>
                        <option value="/?type=month" {{$type == 'month' ? ' selected' : ''}}>Этот месяц</option>
                    </select>
                    По типу
                    <select id="homepageSelectEventType">
                        <option value="/" {{$type == '/' ? ' selected' : ''}}>Все события</option>
                        <option value="/?type=interview" {{$type == 'interview' ? ' selected' : ''}}>Интервью</option>
                        <option value="/?type=worker" {{$type == 'worker' ? ' selected' : ''}}>Сотрудники</option>
                        <option value="/?type=candidate" {{$type == 'candidate' ? ' selected' : ''}}>Кандидаты</option>
                        <option value="/?type=vacancy" {{$type == 'vacancy' ? ' selected' : ''}}>Вакансии</option>
                    </select>
                    {{--<a href="/"><button class="btn btn-sm {{ $interval == '' ? ' btn-dark' : ' btn-outline-dark' }}">Все события</button></a>
                    <a href="/?interval=today"><button class="btn btn-sm {{ $interval == 'today' ? ' btn-danger' : ' btn-outline-danger' }}">Сегодня</button></a>
                    <a href="/?interval=yesterday"><button class="btn btn-sm {{ $interval == 'yesterday' ? ' btn-warning' : ' btn-outline-warning' }}">Вчера</button></a>
                    <a href="/?interval=week"><button class="btn btn-sm {{ $interval == 'week' ? ' btn-primary' : ' btn-outline-primary' }}">Эта неделя</button></a>
                    <a href="/?interval=month"><button class="btn btn-sm {{ $interval == 'month' ? ' btn-success' : ' btn-outline-success' }}">Этот месяц</button></a>--}}

                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{--You are logged in!--}}

                    @include('home._home-event-list')

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
