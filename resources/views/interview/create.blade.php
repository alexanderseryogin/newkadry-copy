@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Создание нового интервью
                    <a href="{{ route('interview.index') }}" class="close">
                        <span aria-hidden="true">&times;</span>
                    </a>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('interview.store') }}" enctype="multipart/form-data">
                        @csrf

                        @include('interview._interview-create-form-fields')

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection