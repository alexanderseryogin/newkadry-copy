<div class="form-group">
    <label for="exampleFormControlSelect1">Кандидат</label>
    <select id="candidate" class="form-control{{ $errors->has('position') ? ' is-invalid' : '' }}" name="candidate_id" required>
        <option value="">Выбрать кандидата...</option>

        {{ $i = 0 }}
        @foreach ($data->candidatesList as $key => $value)
                <option value="{{ $data->candidatesList[$i]->user->id }}"{{ $key == old('position') ? ' selected' : '' }}>
                    {{ $data->candidatesList[$i]->user->first_name }}
                    {{ $data->candidatesList[$i]->user->second_name }}
                    {{ $data->candidatesList[$i]->user->last_name }}
                    &nbsp;&nbsp;
                    ({{ $data->candidatesList[$i]->city->title }},&nbsp;
                    {{ $data->candidatesList[$i]->position->title }})
                </option>
        {{ $i++ }}
        @endforeach

    </select>
</div>
<div class="form-group">
        <label for="exampleFormControlSelect1">Позиция</label>
        <select id="position" class="form-control{{ $errors->has('position') ? ' is-invalid' : '' }}" name="position_id" required>
            <option value="">Выбрать позицию...</option>
            @foreach ($data->positionsList as $key => $value)
                <option value="{{ $key }}"{{ $key == old('position') ? ' selected' : '' }}>
                    {{ $value }}
                </option>
            @endforeach
        </select>
</div>
<div class="form-group">
    <label for="exampleFormControlSelect1">Город</label>
    <select id="city_id" name="city_id" class="select_city form-control{{ $errors->has('position') ? ' is-invalid' : '' }}" name="city_id" required>
        <option value="">Выбрать город...</option>
        @foreach ($data->citiesList as $key => $value)
            <option value="{{ $key }}"{{ $key == old('position') ? ' selected' : '' }}>
                {{ $value }}
            </option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="exampleFormControlSelect1">Вакансия</label>
    <select id="vacancy_id" name="vacancy_id" class="form-control{{ $errors->has('position') ? ' is-invalid' : '' }}" name="vacancy_id" required>
        <option value="">Выбрать вакансию...</option>
        @foreach ($data->vacanciesList as $key => $value)
            <option value="{{ $key }}"{{ $key == old('position') ? ' selected' : '' }}>
                {{ $value }}
            </option>
        @endforeach
    </select>
</div>

 <div class="form-group">
     <label for="exampleFormControlSelect2">Наши участники интервью</label>
     <br>
         @for ($i = 0; $i < count($data->usersHRList); $i++)
             <input
                     class="interviewers_list"
                     name="interviewer_id[]"
                     type="checkbox"
                     value="{{ $data->usersHRList[$i]->id }}"
                     {{ $data->usersHRList[$i]->id == 1 ? ' checked' : '' }}
                     {{ $key == old('position') ? ' checked' : '' }}
             >
                 {{ $data->usersHRList[$i]->first_name  }} {{ $data->usersHRList[$i]->last_name }}
             <br>
         @endfor
 </div>

 <div class="row">
     <div class="col">
         <label for="date">Дата интервью</label>
         <input id="date" type="date" class="form-control" name="date_from" required>
     </div>
     <div class="col">
         <label for="time_begin">Время начала</label>
         <input id="time" type="time" class="form-control" name="date_to" required>
     </div>
 </div>

{{--<h3>Inline</h3>
<div id="date_picker"> </div>
<script type="text/javascript">
    $(function(){
        $('#date_picker').dtpicker();
    });
</script>--}}

<script>
    $(function () {
        // autoselect default interviewers by branch
        $('select#position').change(function () {
            let positionID = $('select#position :selected').val();
            // console.log(positionID);
            if(positionID == 1)
            {
                $('input.interviewers_list[value="2"]').attr('checked', 'checked'); // PHP - Алексей Доценко
                    //remove specialists on another branches
                $('input.interviewers_list[value="3"]').removeAttr('checked');
                $('input.interviewers_list[value="4"]').removeAttr('checked');
            }
            if(positionID == 3)
            {
                $('input.interviewers_list[value="4"]').attr('checked', 'checked'); // Ruby - Михаил Беляев
                    //remove specialists on another branches
                $('input.interviewers_list[value="3"]').removeAttr('checked');
                $('input.interviewers_list[value="2"]').removeAttr('checked');
            }
            if(positionID == 4)
            {
                $('input.interviewers_list[value="3"]').attr('checked', 'checked'); // JS - Дмитрий Оплачко
                    //remove specialists on another branches
                $('input.interviewers_list[value="2"]').removeAttr('checked');
                $('input.interviewers_list[value="4"]').removeAttr('checked');
            }

        });

        // autoselect default HR by city
        $('select.select_city').change(function () {
             var cityID = $('select.select_city').val();
             console.log(cityID);
            if(cityID == 1){ // ZP
                $('input.interviewers_list[value="5"]').attr('checked', 'checked');
                $('input.interviewers_list[value="6"]').removeAttr('checked', 'checked');
            }
            if(cityID == 2){ // Lwow
                $('input.interviewers_list[value="6"]').attr('checked', 'checked');
                $('input.interviewers_list[value="5"]').removeAttr('checked', 'checked');
            }

        });
    });
</script>