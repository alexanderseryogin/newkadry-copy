@extends('layouts.app')

@section('styles')
    <link href="{{ asset('css/datetimepicker.css') }}">
    <link rel="stylesheet" href="{!! asset('css/bootstrap-datetimepicker.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/bootstrap-select.min.css') !!}">
@endsection

@section('content')

    <h1>Добавить событие в календарь</h1>


    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif


    <div class="col-md-3"></div>
    <div class="col-md-6">
        <form action="{{route('calendar.create')}}" method="post">
            <input name="_method" type="hidden" value="PUT">
            @csrf
            <div class="form-group">
                <label for="calendar_title">Заголовок</label>
                @if ($errors->has('title'))
                    <div class="alert alert-danger">{{ $errors->first('title') }}</div>
                @endif
                <input type="text" name="title" class="form-control" id="" aria-describedby="emailHelp" placeholder="Кратко о событии">
                <small id="calendar_title_help" class="form-text text-muted"></small>
            </div>
            <div class="form-group">
                <label for="calendar_description">Текст события</label>
                @if ($errors->has('description'))
                    <div class="alert alert-danger">{{ $errors->first('description') }}</div>
                @endif
                <textarea class="form-control" name="description" id="calendar_description" placeholder="Описание события..." rows="10"></textarea>
            </div>
            <div class="form-group">
                <label for="calendar_event_begin">Начало события</label>
                @if ($errors->has('start'))
                    <div class="alert alert-danger">{{ $errors->first('start') }}</div>
                @endif
                <input type="text" name="start" class="form-control" id="calendar_event_begin" placeholder="<?=date('Y-m-d H:i:s')?>" value="<?=date('Y-m-d H:i:s')?>">
            </div>
            <div class="form-group">
                <label for="calendar_event_end">Конец события</label>
                @if ($errors->has('end'))
                    <div class="alert alert-danger">{{ $errors->first('end') }}</div>
                @endif
                <input type="text" name="end" class="form-control" id="calendar_event_end" placeholder="<?=date('Y-m-d H:i:s')?>" value="<?=date('Y-m-d H:i:s')?>">
            </div>
            <div class="form-group">
                <label for="calendar_event_color">Цвет события</label>
                <input type="color" name="color" class="form-control" id="calendar_event_end">
            </div>

            <button type="submit" class="btn btn-primary">Добавить</button>
        </form>
    </div>
    <div class="col-md-3"></div>



@endsection

@section('scripts')
    <script src="{!! asset('js/jquery.datetimepicker.js') !!}"></script>
    <script src="{!! asset('js/bootstrap-datetimepicker.min.js') !!}"></script>
    <script src="{!! asset('js/bootstrap-select.min.js') !!}"></script>
    <script>
        // jQuery('#calendar_event_begin').datetimepicker();
    </script>
    <!-- this should go after your </body> -->
    <link rel="stylesheet" type="text/css" href="build/css/jquery.datetimepicker.css">
    <script src="/build/js/jquery.datetimepicker.full.min.js"></script>

@endsection

